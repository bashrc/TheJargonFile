smartphone

n. A very annoying type of mobile phone which is often expensive, with poor
battery performance and delivering a constant stream of outrage-inducing
notifications. Considered essential by most people. Rarely used for telephony.
