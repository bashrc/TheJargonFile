religious issues

n. Questions which seemingly cannot be raised without touching off holy wars,
such as What is the best operating system (or editor, language,
architecture, shell, mail reader, news reader)? See holy wars; see
also theology , bigot , and compare rathole. People actually develop the
most amazing and religiously intense attachments to their tools, even
when the tools are intangible.