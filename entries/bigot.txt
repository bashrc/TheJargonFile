bigot

n. [common] A person who is religiously attached to a particular computer,
language, operating system, editor, or other tool (see religious issues).
Among hackers "language bigotry" is fairly common as people religiously
defend their favorite programming language or development environment.
