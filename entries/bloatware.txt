bloatware

n. [common] Software that provides minimal functionality while requiring a
disproportionate amount of diskspace and memory. Especially used for
application and OS upgrades. This term is very common in relation to
Windows operating systems and applications.

Also sometimes used in reverse for humerous effect. "Eleven megabytes?!
TinyCore Linux is bloatware!"
