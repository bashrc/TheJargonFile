evil bit

Humerous proposal in RFC3514 to make life easy for firewalls by setting a TCP
packet bit to indicate if it was transmitted by an evil entity.
"If the bit is set to 1, the packet has evil intent". It's a satire on how
people look for simple technical solutions to what are really complex social
problems (the problem of evil on the internet). Also see the XMPP equivalent
XEP-0076 "Malicious Stanzas".