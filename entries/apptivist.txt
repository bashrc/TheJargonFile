apptivist

A software developer who works on mobile apps aimed at overcoming social
problems. Similar to a hacktivist, but specialised upon mobile apps.