god object

In object oriented programming a this is an object which encapsulates way
too many things. Such objects usually aren't part of the design but evolved
over time as each patch added slightly more code mass to the snowball.