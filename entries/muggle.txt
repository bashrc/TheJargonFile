muggle

A non- wizard. Implies vague pity rather than contempt. In the universe
of Rowling's popular children's series, muggles and wizards inhabit the
same modern world, but each group is ignorant of the commonplaces of
the others' existence most muggles are unaware that wizards exist,
and wizards (used to magical ways of doing everything) are perplexed
and fascinated by muggle artifacts. In retrospect it seems completely
inevitable that hackers would adopt this metaphor, and in hacker
usage it readily forms compounds such as muggle-friendly. Compare
mundane, chainik, newbie, wannabe.
