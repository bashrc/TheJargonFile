blacklist

aka blocklist or denylist. A list of users or domains to be blocked. This can
be to avoid spam or harassment. Also see whitelist.