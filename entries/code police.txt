code police

n. [by analogy with George Orwell's thought police ] A mythical team of
Gestapo-like storm troopers that might burst into one's office and arrest
one for violating programming style rules. Also reminiscent of Turing Police
from William Gibson's Neuromancer who try to prevent AI systems from
breaking out from their constraints.
